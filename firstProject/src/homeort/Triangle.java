package homeort;

public class Triangle {
    public static void main(String[] args) {
        int x;
        int y;

        for (y = 1; y <= 6; y++) {
            if (!(y == 1)) {
                System.out.println();
            }
            for (x = 1; x <= y; x++)
                System.out.print("*");
        }
        System.out.println();
    }
}
