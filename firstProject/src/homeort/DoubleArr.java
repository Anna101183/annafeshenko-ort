package homeort;

import java.util.Random;

public class DoubleArr {
    public static void main (String[] arges) {
        final int rows = 2;
        final int cols = 5;
        int min = 1;
        int max = 20;
        
        int [][] arr = new int [rows][];
        
        for (int i = 0; i<arr.length;i++) {
            arr[i]=new int [cols];
        }
        //заполняем массив рандомными значениеми
        Random r =new Random();
        for(int i=0; i< arr.length;i++) {
            for(int j=0; j<cols;j++) {
                arr[i][j]=r.nextInt((max - min)+1);
            }
        }
        // считаем сумму
        int sum=0;
        for(int i=0; i<arr.length;i++) {
            for (int j=0; j < cols;j++) {
                sum+=arr[i][j];                
            }
        }
        System.out.print(sum);
    }
    

}
