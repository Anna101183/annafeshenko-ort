package homeort;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        // 4.65
        Scanner s = new Scanner(System.in);
        System.out.println("Input number year");
        int n = s.nextInt();

        boolean years = (n % 100 != 0 && n % 4 == 0);
        boolean endZero = (n % 400 == 0);

        if (years || endZero) {
            System.out.println("Thise year is leap year");
        } else {
            System.out.println("Thise year isn't leap year");
        }
    }
}
