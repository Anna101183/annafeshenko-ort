package homeort;

import java.util.Random;

public class AmountOfPupuls {
    public static void main(String[] arges) {
        int[][] students = new int[11][4];
        filltable(students);

        // считаем количество детей по паралелям
        int[] parallel = sumRow(students);
        // Наименьшее количество детей, поиск через массив
        int numberMinArr = minArr(parallel);
        // Наибольшее количество детей поиск через массив
        int numberMaxArr = maxArr(parallel);

        // Наименьшее количество детей поиск без массива
        int numberMin = min(students);

        // наибольшее количество детей, поиск без массива
        int numberMax = max(students);

        System.out.println("В паралели " + numberMinArr + " классов наименьшее количество детей");
        System.out.println("В паралели " + numberMaxArr + " классов наибольшее количество детей");
        // без массива
        System.out.println("В паралели " + numberMin + " классов наименьшее количество детей");
        System.out.println("В паралели " + numberMax + " классов наибольшее количество детей");
    }

    static void filltable(int[][] table) {
        Random r = new Random();
        int low = 20;
        int high = 30;

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                table[i][j] = r.nextInt(high - low) + low;
            }
        }
    }

    static int[] sumRow(int[][] table) {
        int[] parallel = new int[table.length];

        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                parallel[i] += table[i][j];
            }
        }
        return parallel;
    }

    static int minArr(int[] minArr) {
        return minMaxArr(minArr, true);
    }

    static int maxArr(int[] maxArr) {
        return minMaxArr(maxArr, false);
    }

    static int minMaxArr(int[] minMaxArr, boolean isMin) {
        int amountMinMax = minMaxArr[0];
        int numberMinMax = 1;
        for (int i = 0; i < minMaxArr.length; i++) {
            if (minMaxArr[i] < amountMinMax && isMin || minMaxArr[i] > amountMinMax && !isMin) {
                amountMinMax = minMaxArr[i];
                numberMinMax = i + 1;
            }
        }
        return numberMinMax;
    }

    static int min(int[][] min) {
        return minMax(min, true);
    }

    static int max(int[][] max) {
        return minMax(max, false);
    }

    static int minMax(int[][] minMax, boolean isMin) {
        int countMinMax = 0;
        int countNumberMinMax = 0;
        int sum = 0;
        for (int i = 0; i < minMax.length; i++) {
            sum = 0;

            for (int j = 0; j < minMax[i].length; j++) {
                sum += minMax[i][j];
            }
            if (i == 0) {
                countMinMax = sum;
                countNumberMinMax = i + 1;
            } else if (countMinMax > sum && isMin || countMinMax < sum && !isMin) {
                countMinMax = sum;
                countNumberMinMax = i + 1;
            }
        }
        return countNumberMinMax;
    }
}
