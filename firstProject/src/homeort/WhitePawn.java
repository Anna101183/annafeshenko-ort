package homeort;

public class WhitePawn {
    public static void main(String[] args) {
        // 3,34d white pawn
        int a = 3;
        int b = 6;
        int c = 4;
        int d = 6;

        boolean r = (b == d) && (a != 1 && c == a + 1) || (a == 2 && c == a + 2);

        System.out.println(r);
    }
}
