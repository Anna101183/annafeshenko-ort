package homeort;

import java.util.Scanner;

public class TrafficLight {
    public static void main(String[] args) {
        // 4.36
        Scanner s = new Scanner(System.in);
        System.out.println("Input value minutes from 1 to 60 ");
        int t = s.nextInt();

        final int z = 3;
        final int r = 2;

        if (t <= 0 || t > 60) {
            System.out.println("Incorrect value");
            return;
        }

        int ost = t % (3 + 2);

        if (ost != 0 && ost <= z) {
            System.out.println("Green ");
        } else {
            System.out.println("Red ");
        }
    }
}
