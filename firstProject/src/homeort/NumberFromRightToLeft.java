package homeort;

import java.util.Scanner;

public class NumberFromRightToLeft {
    public static void main(String[] args) {
        //�2.20� number from right to left
        Scanner s = new Scanner(System.in);
        System.out.println("Input four-digit number  ");
        int value = s.nextInt();

        int first = value % 10;
        int second = value % 100 / 10;
        int third = value % 1000 / 100;
        int fourth = value / 1000;
        
    int result = first * 1000 + second * 100 + third * 10 + fourth;
        
        System.out.print(result);
    }

}
