package homeort;

public class Blackpawn {
    public static void main(String[] args) {
        // 3,34E Black pawn
        int a = 7;
        int b = 6;
        int c = 5;
        int d = 6;

        boolean r = (b == d) && (a != 8 && c == a - 1) || (a == 7 && c == a - 2);

        System.out.println(r);
    }
}
