package homeort;

import java.text.DecimalFormat;

public class Price {
    public static void main(String[] args) {
        double distance = 10.00;
        double koef = (100 + 10) / 100.;
        int day = 1;
        double distanceSevenDay = 10;
        while (day <= 10) {
            if (day == 1) {
                System.out.println("|" + day + "|" + distance + "|");
                ++day;
                continue;
            }
            double change = distance * koef;
            distance = change;
            DecimalFormat f = new DecimalFormat("##.0");
            System.out.println("|" + day + "|" + (f.format(change)) + "|");
            if (day <= 7) {
                distanceSevenDay += change;
            }
            ++day;
        }
        DecimalFormat f = new DecimalFormat("##.00");
        System.out.println("Person have traffic for 7 days " + (f.format(distanceSevenDay)) + "km");
    }

}
