package homeort;

public class PointOOP {
    public static void main(String[] arges) {
        Point a = new Point(5,0,"a");
       
        Point b = new Point(2,4,"b");
       
        Point c = new Point(3,1,"c");
       
        Point[] points = { a, b, c };
        sortByMin(points);
        print(points);

    }

    static void print(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            System.out.printf("%s%n", points[i].name);
        }
    }

    static void sortByMin(Point[] points) {
        for (int i = points.length; i > 1; i--) {
            for (int j = 1; j < i; j++) {
                if (distance(points[j - 1]) > distance(points[j])) {
                    Point temp = points[j - 1];
                    points[j - 1] = points[j];
                    points[j] = temp;
                }
            }
        }
    }

    static double distance(Point point) {
        double distance = Math.sqrt((Math.pow(point.x, 2) + Math.pow(point.y, 2)));
        return distance;
    }

}

class Point {
    int x;
    int y;
    String name;

    Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

}
