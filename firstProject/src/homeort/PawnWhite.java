package homeort;

public class PawnWhite {
    public static void main(String[] args) {
        // 3,34E Pawn white battle
        int a = 2;
        int b = 4;
        int c = 3;
        int d = 4;

        boolean r = (a != 1 && c == a + 1) && Math.abs(b - d) == 1;

        System.out.println(r);
    }
}
