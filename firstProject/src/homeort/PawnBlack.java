package homeort;

public class PawnBlack {
    public static void main(String[] args) {
        // 3,34d black pawn battle
        int a = 3;
        int b = 4;
        int c = 2;
        int d = 3;

        boolean r = (a != 8 && c == a - 1) && Math.abs(b - d) == 1;

        System.out.println(r);
    }
}
