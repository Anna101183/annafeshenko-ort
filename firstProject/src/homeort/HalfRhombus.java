package homeort;

public class HalfRhombus {
    public static void main(String[] args) {

        int x;
        int y;
        int n = 4;

        for (y = 1; y < n; y++) {
            for (x = 1; x <= y; x++)
                if (x == 1 || x == y) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            System.out.println();
        }
        if (y == n) {
            for (x = 1; x <= y; x++) {
                System.out.print("*");
            }
            System.out.println();

        }
        for (y = n - 1; y > 0; y--) {
            for (x = 1; x <= y; x++)
                if (x == 1 || x == y) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            System.out.println();
        }
    }
}
