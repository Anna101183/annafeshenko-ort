package homeort;

import java.util.Random;

public class Winter {
	public static void main(String[] arges) {
		int[] days = new int[365];
		int[] winds = { 0, 0, 0, 0, 0, 0, 0, 0 };
		Random r = new Random();
		int high = 9;
		int low = 1;
		for (int i = 0; i < days.length; i++) {
			days[i] = r.nextInt(high - low) + low;
		}

		for (int i = 0; i < days.length; i++) {
			winds[days[i] - 1]++;
		}
		int min = winds[0];
		int minIndex = 0;
		for (int i = 0; i < winds.length; i++) {
			if (winds[i] < min) {
				min = winds[i];
				minIndex = i;
			}
		}
		switch (minIndex) {
		case 0:
			System.out.println("North");
			break;
		case 1:
			System.out.println("South");
			break;
		case 2:
			System.out.println("East");
			break;
		case 3:
			System.out.println("West");
			break;
		case 4:
			System.out.println("North-West");
			break;
		case 5:
			System.out.println("North-East");
			break;
		case 6:
			System.out.println("South-West");
			break;
		case 7:
			System.out.println("South-East");
			break;
		}
	}
}