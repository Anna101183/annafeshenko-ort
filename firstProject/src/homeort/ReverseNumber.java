package homeort;

import java.util.Scanner;

public class ReverseNumber {
    public static void main(String[] args) {
        System.out.println("Enter number:  ");
        revers();
    }

    private static void revers() {
        Scanner s = new Scanner(System.in);
        int number = s.nextInt();
        if (number != 0) {
            revers();
            s.close();
        }
        System.out.println(number);
    }

}
