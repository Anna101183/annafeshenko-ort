package homeort;

public class Ht2PrimitiveValue {
    public static void main(String[] args) {
        byte min = -128;
        short twoMin = -32768;
        int fourMin = -2147483648;
        long eightMin = -9223372036854775808L;
        float decimal = -1.4e-045f;
        char symbol = 'A';
        boolean condition = true;

        System.out.println("First value:");
        System.out.println(min);
        System.out.println(twoMin);
        System.out.println(fourMin);
        System.out.println(eightMin);
        System.out.println(decimal);
        System.out.println(symbol);
        System.out.println(condition);

        min = 127;
        twoMin = 32767;
        fourMin = 2147483647;
        eightMin = 9223372036854775807L;
        decimal = 3.4e+038f;
        symbol = 'R';
        condition = false;

        System.out.println("Modified value:");
        System.out.println(min);
        System.out.println(twoMin);
        System.out.println(fourMin);
        System.out.println(eightMin);
        System.out.println(decimal);
        System.out.println(symbol);
        System.out.println(condition);
    }

}
