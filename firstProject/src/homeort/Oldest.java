package homeort;

import java.util.Random;

public class Oldest {
    public static void main(String[] arges) {
        int[] years = new int[30];
        int i, old1, old2, interjacent;
        Random r = new Random();
        int low = 1800;
        int high = 1900;
        for (i = 0; i < years.length; i++) {
            years[i] = r.nextInt(high - low) + low;
        }
        if (years[0] < years[1]) {
            old1 = 0;
            old2 = 1;
        } else {
            old1 = 1;
            old2 = 0;
        }
        for (i = 2; i < years.length; i++) {
            if (years[i] < years[old1]) {
                interjacent = old1;
                old1 = i;
                if (years[interjacent] < years[old2]) {
                    old2 = interjacent;
                }
            } else {
                if (years[i] < years[old2]) {
                    old2 = i;
                }
            }
        }
        System.out.println("The oldest was born " + years[old1] + " year");
        System.out.println("Person who born " + years[old2] + " has second place  ");
    }
}