package com.ort.javaadvanced2019.entities;

import java.util.Arrays;

public enum Gender {

	FEMALE("Female"), MALE("Male");

	private final String gender;

	private Gender(String title) {
		gender = title;
	}

	public String getString() {
		return gender;
	}

	public static Gender getGenderBy (String description) {
		return Arrays.stream(Gender.values()).filter(gender -> gender.gender.equals(description)).findFirst()
				.orElse(null);
	}
}
