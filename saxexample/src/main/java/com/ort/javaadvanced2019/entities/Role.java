package com.ort.javaadvanced2019.entities;

import java.util.Arrays;

public enum Role {

	JAVA_DEVELOPER("Java Developer"), CEO("CEO"), MANAGER("Manager");

	private final String role;

	private Role(String title) {
		role = title;
	}

	public String getString() {
		return role;
	}

	public static Role getRoleBy(String description) {
		return Arrays.stream(Role.values()).filter(role -> role.role.equals(description)).findFirst().orElse(null);
	}

}
