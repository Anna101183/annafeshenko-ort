package com.ort.javaadvanced2019.entities;

public class Employee {
	private int _id;
	private int _age;
	private String _name;
	private Gender _gender;
	private Role _role;
		
	public int getId() {
		return _id;
	}
	
	public void setId(int id) {
		_id = id;
	}
	
	public int getAge() {
		return _age;
	}
	
	public void setAge(int age) {
		_age = age;
	}
	
	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		_name = name;
	}
	
	public Gender getGender() {
		return _gender;
	}
	
	public void setGender(Gender gender) {
		_gender = gender;
	}
	
	public Role getRole() {
		return _role;
	}
	
	public void setRole(Role role) {
		_role = role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _age;
		result = prime * result + ((_gender == null) ? 0 : _gender.hashCode());
		result = prime * result + _id;
		result = prime * result + ((_name == null) ? 0 : _name.hashCode());
		result = prime * result + ((_role == null) ? 0 : _role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (_age != other._age)
			return false;
		if (_gender != other._gender)
			return false;
		if (_id != other._id)
			return false;
		if (_name == null) {
			if (other._name != null)
				return false;
		} else if (!_name.equals(other._name))
			return false;
		if (_role != other._role)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [_id=" + _id + ", _age=" + _age + ", _name=" + _name + ", _gender=" + _gender + ", _role="
				+ _role + "]";
	}
	
}
