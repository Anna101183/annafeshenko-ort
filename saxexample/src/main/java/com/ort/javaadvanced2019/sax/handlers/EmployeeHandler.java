package com.ort.javaadvanced2019.sax.handlers;

import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.ATTR_ID;
import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.EMPLOYEE;
import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.AGE;
import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.NAME;
import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.GENDER;
import static com.ort.javaadvanced2019.sax.handlers.EmployeeXMLConstants.ROLE;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import com.ort.javaadvanced2019.entities.Gender;
import com.ort.javaadvanced2019.entities.Role;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.ort.javaadvanced2019.entities.Employee;

public class EmployeeHandler extends DefaultHandler {

	private List<Employee> _result = new LinkedList<>();
	private Employee _temp;
	private String _lastField = null;
	private String stringParse = null;

	@Override
	public void startDocument() throws SAXException {
		System.out.println("Starting....");
	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub
		System.out.println("Finishing....");
	}

	@Override // qName - �������� ��������
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		_lastField = qName;
		if (EMPLOYEE.equals(qName)) {
			_temp = new Employee();
			int id = Integer.parseInt(attributes.getValue(ATTR_ID));
			_temp.setId(id);
		}

		System.out.println();// attributes.getValue("id");
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (EMPLOYEE.equals(qName)) {
			_result.add(_temp);
			_temp = null;
		}
		_lastField = null;
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		stringParse = new String(ch).substring(start, (start + length));
		parserEmployee(_lastField, stringParse);
		super.characters(ch, start, length);
	}

	public void parserEmployee(String lastField, String stringParse) {

		if (AGE.equals(lastField)) {
			int age = Integer.parseInt(stringParse);
			_temp.setAge(age);
		}
		if (NAME.equals(lastField)) {
			String name = stringParse;
			_temp.setName(name);
		}
		if (GENDER.equals(lastField)) {
			Gender enumGender = Gender.getGenderBy(stringParse);
			if (enumGender == null) {
				System.out.println("Gender is not found");
			}
			_temp.setGender(enumGender);
		}

		if (ROLE.equals(lastField)) {
			Role enumRole = Role.getRoleBy(stringParse);
			if (enumRole == null) {
				System.out.println("Role is not found");
			}
			_temp.setRole(enumRole);
		}
	}

	public List<Employee> getResult() {
		return _result;
	}

}
