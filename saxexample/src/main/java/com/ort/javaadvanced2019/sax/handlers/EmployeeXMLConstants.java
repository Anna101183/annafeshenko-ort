package com.ort.javaadvanced2019.sax.handlers;

public interface EmployeeXMLConstants {
	String EMPLOYEE      = "Employee";
	String ATTR_ID       = "id";
	String AGE           = "age";
	String NAME          = "name";
	String GENDER        = "gender";
	String ROLE          = "role";
	String GENDER_MALE   = "Male";
	String GENDER_FEMALE = "Female";
	String JAVA_DEVELOPER = "Java Developer";
	String ROLE_CEO      = "CEO";
	String ROLE_MANAGER  = "Manager";
}
