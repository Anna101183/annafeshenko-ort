package com.ort.javaadvanced2019;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.ort.javaadvanced2019.entities.Employee;
import com.ort.javaadvanced2019.sax.handlers.EmployeeHandler;

public class Main {	
	public static void main(String[] args) {
		EmployeeHandler handler = new EmployeeHandler();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File("emplyees.xml"), handler);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Employee> result = handler.getResult(); 
		System.out.println(result);
	}
}
