package com.ort.strazhkoe.dao;

import java.util.List;

import com.ort.strazhkoe.entities.Point;

public interface PointDAO {

	public List<Point> getAll();
	
	public void saveAll(List<Point> points);
}
