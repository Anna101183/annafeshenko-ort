package com.ort.strazhkoe.services;

import java.util.List;

import com.ort.strazhkoe.entities.Point;

public interface PointService {
	
	public List<Point> getAll();
	
	public void saveAll(List<Point> points);
	
	public void add(Point point);
	
	public void delete(int index);
	
	public void delete(Point point);
}
