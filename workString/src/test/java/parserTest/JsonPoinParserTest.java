package parserTest;


import org.junit.Assert;
import org.junit.Test;
import Parser.JsonPoinParser;
import Parser.PointParser;
import pointConstructor.Point;

public class JsonPoinParserTest {

    @Test
    public void testPointParserXYName() {
        Point samplePoint = new Point(15, 40, "test");
        String text = "Point { x:15, y:40, name:test}";
        PointParser pointParser = new JsonPoinParser();
        Point point = pointParser.parse(text);
        Assert.assertEquals(samplePoint, point);
    }

    @Test
    public void testPointParserYXName() {
        Point samplePoint = new Point(15, 40, "test");
        String text = "Point { y:40, x:15, name:test}";
        JsonPoinParser pointParser = new JsonPoinParser();
        Point point = pointParser.parse(text);
        Assert.assertEquals(samplePoint, point);
    }

    @Test
    public void testPointParserYX() {
        Point samplePoint = new Point(15, 40, "zero");
        String text = "Point { y:40, x:15, }";
        JsonPoinParser pointParser = new JsonPoinParser();
        Point point = pointParser.parse(text);
        Assert.assertEquals(samplePoint, point);

    }
}