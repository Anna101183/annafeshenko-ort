package Parser;

import pointConstructor.Point;

public interface PointParser {
    public Point parse(String text);

}
