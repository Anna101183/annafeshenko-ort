package Parser;

import pointConstructor.Point;

public class JsonPoinParser implements PointParser {


    @Override
    public Point parse(String text) {
        String cutText = text.substring(text.indexOf('{') + 1, text.indexOf('}'));
        String[] arreyLine = cutText.split(",");
        Point point = new Point(JsonPoinParser.createX(arreyLine), JsonPoinParser.createY(arreyLine),
                JsonPoinParser.createName(arreyLine, cutText));
        return point;
    }

    private static String createName(String[] arreyLine, String string) {
        String name = null;
        boolean nameIsInTheString = string.contains("name");
        if (nameIsInTheString == false) {
            return name = "zero";
        } else {
            for (int i = 0; i < arreyLine.length; i++) {
                if (arreyLine[i].trim().startsWith("name")) {
                    name = arreyLine[i].trim().substring(arreyLine[i].indexOf(":"), arreyLine[i].length() - 1);
                }
            }
            return name;
        }
    }

    private static int createX(String[] arreyLine) {
        String valueX = null;
        for (int i = 0; i < arreyLine.length; i++) {
            if (arreyLine[i].trim().startsWith("x")) {
                valueX = arreyLine[i].trim().substring(arreyLine[i].indexOf(":"), arreyLine[i].length() - 1);
            }
        }
        return Integer.parseInt(valueX);
    }

    private static int createY(String[] arreyLine) {
        String valueY = null;
        for (int i = 0; i < arreyLine.length; i++) {
            if (arreyLine[i].trim().startsWith("y")) {
                valueY = arreyLine[i].trim().substring(arreyLine[i].indexOf(":"), arreyLine[i].length() - 1);
            }
        }
        return Integer.parseInt(valueY);
    }

    

}